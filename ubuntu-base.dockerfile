# syntax=docker/dockerfile:1
FROM ubuntu:22.04 AS base
RUN apt update
RUN apt install -y python3 python3-pip bats git

FROM base AS production
RUN pip install numpy pandas netcdf4 xarray zarr h5netcdf
WORKDIR /oceanweather
RUN git clone https://gitlab.com/oceanweather/Python-Scripts/wind/winpre_to_nws13.git
RUN git clone https://gitlab.com/oceanweather/Python-Sub/owiWinPre.git
ENV PYTHONPATH=/oceanweather

COPY convert /oceanweather/convert
RUN chmod a+x /oceanweather/convert
ENV PATH=/oceanweather:${PATH}

COPY entrypoint.sh /entrypoint.sh
RUN chmod a+x /entrypoint.sh

# ENTRYPOINT ["bash", "-c"]
CMD bash /entrypoint.sh

RUN bash /entrypoint.sh
