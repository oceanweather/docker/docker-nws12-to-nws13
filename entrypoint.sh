#!/bin/bash 
cat /oceanweather/winpre_to_nws13/README.md | sed s/#\ winpre_to_nws13/#\ \*nws12-to-nws13\*\ Container\ Image/g
echo " "
echo " "
echo "## Using Container's *convert* command"
echo "Call with the following (in place of \`python winpre_to_nws13.py\`):"
echo " "
echo "\`\`\`"
echo "$ docker run registry.gitlab.com/oceanweather/docker/docker-nws12-to-nws13 convert {OPTIONS}"
echo "\`\`\`"
echo " "
echo " "