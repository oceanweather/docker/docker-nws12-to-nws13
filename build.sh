#!/bin/bash

VER=$(date +%Y%m%d)
PROJECT=nws12-to-nws13
DOCKERFILE=ubuntu-base.dockerfile
GITLABREPO=registry.gitlab.com/oceanweather/docker/docker-${PROJECT}
REPO=${GITLABREPO}

select v in docker-build "docker-build (no-cache)" push-image git-commit exit
do
    if [ "$v" == "exit" ]
    then
        exit
    fi

    if [ "$v" == "docker-build" ]
    then
        DOCKER_BUILDKIT=1 docker build --rm --tag $REPO/oceanwx/${PROJECT}:latest -f ${DOCKERFILE} . \
            && docker tag $REPO/oceanwx/${PROJECT}:latest $REPO/oceanwx/${PROJECT}:$VER \
            && docker run registry.gitlab.com/oceanweather/docker/docker-nws12-to-nws13/oceanwx/nws12-to-nws13:latest \
            && docker run registry.gitlab.com/oceanweather/docker/docker-nws12-to-nws13/oceanwx/nws12-to-nws13:latest > README.md
        
    fi

    if [ "$v" == "docker-build (no-cache)" ]
    then
        DOCKER_BUILDKIT=1 docker build --rm --no-cache --tag $REPO/oceanwx/${PROJECT}:latest -f ${DOCKERFILE} . \
            && docker tag $REPO/oceanwx/${PROJECT}:latest $REPO/oceanwx/${PROJECT}:$VER \
            && docker run registry.gitlab.com/oceanweather/docker/docker-nws12-to-nws13/oceanwx/nws12-to-nws13:latest \
            && docker run registry.gitlab.com/oceanweather/docker/docker-nws12-to-nws13/oceanwx/nws12-to-nws13:latest > README.md
    fi

    if [ "$v" == "push-image" ]
    then
        docker images | grep ${PROJECT} | head
        read -p "Please enter Docker image tag:   " TAG
        docker push $REPO/oceanwx/${PROJECT}:latest && \
        docker push $REPO/oceanwx/${PROJECT}:$TAG
    fi

    if [ "$v" == "git-commit" ]
    then
        docker images | grep ${PROJECT} | head
        read -p "Please enter Docker image tag:   " TAG
        read -p "Provide commit message for these changes and docker image version:   " message
        git commit -a -m "${message} for tag ${TAG}" && \
        git diff && \
        git push origin master && \
        git tag $TAG && \
        git push --tags origin
    fi

done
