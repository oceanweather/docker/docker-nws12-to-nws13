# *nws12-to-nws13* Container Image

```
usage: winpre_to_nws13.py [-h] -w WIN [WIN ...] -p PRE [PRE ...] -o OUTPUT
                          [-g GROUP [GROUP ...]] [-a] [-v]

Convert a series of OWI WIN/PRE files to an ADCIRC NWS13 Netcdf file.

optional arguments:
  -h, --help            show this help message and exit
  -w WIN [WIN ...], --win WIN [WIN ...]
                        Input WIN file(s)
  -p PRE [PRE ...], --pre PRE [PRE ...]
                        Input PRE file(s)
  -o OUTPUT, --output OUTPUT
                        Output file path
  -g GROUP [GROUP ...], --group GROUP [GROUP ...]
                        Input group name(s)
  -a, --append          If set, do not clobber the existing file, but append groups to it.
  -v, --verbose         Verbose log output
```

## Example

To convert a single grid:

```bash
python winpre_to_nws13.py \
  -w "/home/alex/Documents/Python/owiWinPre/201304_1DegNATL.Win" \
  -p "/home/alex/Documents/Python/owiWinPre/201304_1DegNATL.Pre" \
  -g Main -v -o test.nc

# Namespace(append=False, group=['Main'], output='test.nc', pre=['/home/alex/Documents/Python/owiWinPre/201304_1DegNATL.Pre'], verbose=True, win=['/home/alex/Documents/Python/owiWinPre/201304_1DegNATL.Win'])
# Reading Main, Rank 1 WIN
# Reading Main, Rank 1 PRE
# Writing Main, Rank 1 to test.nc with mode=w
# Writing global attributes, group_order='Main'
```

To convert multiple grids/overlays:

```bash
python winpre_to_nws13.py \
  -w "/home/alex/Documents/Python/owiWinPre/201304_1DegNATL.Win" \
     "/home/alex/Documents/Python/owiWinPre/201304_1DegNATL.Win" \
  -p "/home/alex/Documents/Python/owiWinPre/201304_1DegNATL.Pre" \
     "/home/alex/Documents/Python/owiWinPre/201304_1DegNATL.Pre" \
  -g Main TestStorm -v -o test.nc

# Namespace(append=False, group=['Main', 'TestStorm'], output='test.nc', pre=['/home/alex/Documents/Python/owiWinPre/201304_1DegNATL.Pre', '/home/alex/Documents/Python/owiWinPre/201304_1DegNATL.Pre'], verbose=True, win=['/home/alex/Documents/Python/owiWinPre/201304_1DegNATL.Win', '/home/alex/Documents/Python/owiWinPre/201304_1DegNATL.Win'])
# Reading Main, Rank 1 WIN
# Reading Main, Rank 1 PRE
# Writing Main, Rank 1 to test.nc with mode=w
# Reading TestStorm, Rank 2 WIN
# Reading TestStorm, Rank 2 PRE
# Writing TestStorm, Rank 2 to test.nc with mode=a
# Writing global attributes, group_order='Main TestStorm'
```
 
 
## Using Container's *convert* command
Call with the following (in place of `python winpre_to_nws13.py`):
 
```
$ docker run registry.gitlab.com/oceanweather/docker/docker-nws12-to-nws13 convert {OPTIONS}
```
 
 
